package pages;

import org.openqa.selenium.WebElement;

import wdMethods.ProjectMethods;

public class FindLeadPage extends ProjectMethods{
	
	public FindLeadPage typeLeadFirstName(String data) {
		WebElement eleLeadFirstName = locateElement("xpath", "(//input[@name='firstName'])[3]");
		type(eleLeadFirstName, data);
		return this; 
	
	}
	
	public FindLeadPage clickFindLeads() {
		WebElement eleFindLeads = locateElement("xpath", "//button[text()='Find Leads']");
		click(eleFindLeads);
		return this;
	}
	
	public ViewLeadPage clickFirstResult() {
		WebElement eleFirstResult = locateElement("xpath", "(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a");
		click(eleFirstResult);
		return new ViewLeadPage();
	}
}
