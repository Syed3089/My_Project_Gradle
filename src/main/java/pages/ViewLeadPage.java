package pages;

import org.openqa.selenium.WebElement;

import wdMethods.ProjectMethods;

public class ViewLeadPage extends ProjectMethods{
	
	public ViewLeadPage clickEdit() {
		WebElement eleClickEdit = locateElement("xpath", "//a[text()='Edit']");
		click(eleClickEdit);
		return this; 
	}
	
	public ViewLeadPage changeCompanyName(String data) {
		WebElement eleCompanyName = locateElement("id", "updateLeadForm_companyName");
		type(eleCompanyName, data);
		return this;
	}

	public ViewLeadPage saveChanges() {
		WebElement eleSaveChanges = locateElement("xpath", "(//input[@type='submit'])[1]");
		click(eleSaveChanges);
		return this;
		
		
	}
}
