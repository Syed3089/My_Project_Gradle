package tests;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import pages.MyHomePage;
import wdMethods.ProjectMethods;

public class TC002_EditLead extends ProjectMethods {
	
	@BeforeClass
	public void setData() {
		testCaseName = "TC001_CreateLead";
		testCaseDescription ="Create a lead";
		category = "Smoke";
		author= "Babu";
		dataSheetName="TC001";
	}
	
	@Test(dataProvider="fetchData")
	public  void createLead(String firstname,String cname)   {
		new MyHomePage()
		.clickLeads()
		.clickFindLead()
		.typeLeadFirstName(firstname)
		.clickFindLeads()
		.clickFirstResult()
		.clickEdit()
		.changeCompanyName(cname)
		.saveChanges();
	}
	

}
